From: Ole Streicher <olebole@debian.org>
Date: Wed, 15 Feb 2017 12:23:04 +0100
Subject: Fix build.xml for use outside of starjava

This includes the following changes:

 * Rename the `extclasspath` elements to `path`. With ant >= 1.6, there is no
   difference (and before, the difference was not relevant for Debian)

 * Ignore local property files

 * Change the starlink installation dir to the standard Java path /usr/share/java/

 * Prefix the name by `starlink-`

 * Adjust the build and test classpaths for Debian (also for CI tests)

 * Set a class path in the jar manifest

 * Set the source file encoding (cp1252), but not the source version (deprecated)

 * Don't sign the jarfile
---
 build.xml | 93 ++++++++++++++++++++++++---------------------------------------
 1 file changed, 35 insertions(+), 58 deletions(-)

diff --git a/build.xml b/build.xml
index a811132..9ad3edf 100644
--- a/build.xml
+++ b/build.xml
@@ -32,12 +32,6 @@
  !-->
 
 <project name="Build file for Datanode" default="build" basedir=".">
-
-  <!-- If either or both of these files exist then any properties
-   !   contained within them will override those defined here.  -->
-  <property file="${user.home}/.stardev.properties"/>
-  <property file=".properties"/>
-
   <!-- Properties will also be set for all environment variables
    !   (PATH becomes "env.PATH"), generally not a good
    !   idea as names are OS dependent -->
@@ -50,13 +44,13 @@
    !-->
 
   <!-- Directory for the Starlink installation (usually /star/java)-->
-  <property name="star.dir" value="${basedir}/../../"/>
+  <property name="star.dir" value="/usr/share/java"/>
 
   <!-- Directory to install into (install target, usually /star/java)-->
   <property name="star.install" value="${star.dir}"/>
 
   <!-- Directory that contains the Starlink jar tree -->
-  <property name="star.jar.dir" value="${star.dir}/lib"/>
+  <property name="star.jar.dir" value="${star.dir}"/>
 
   <!-- Directory that contains the locally built sources (usually
    !   /star/java/source for full distribution) -->
@@ -77,8 +71,8 @@
    !-->
 
   <!-- Define the package name and current versions -->
-  <property name="Name" value="Datanode"/>
-  <property name="name" value="datanode"/>
+  <property name="Name" value="Starjava Datanode"/>
+  <property name="name" value="starlink-datanode"/>
   <property name="version" value="1.0"/>
 
   <!-- The Java package name -->
@@ -114,6 +108,7 @@
   <!-- Directory containing any third-party jars that should be
    !   distributed (normally these would belong in a proper package)-->
   <property name="src.jars.dir" value="${src.dir}/lib"/>
+  <mkdir dir="${src.jars.dir}"/>
 
   <!-- Directory containing any JNI source code -->
   <property name="src.jni.dir" value="${src.dir}/jni"/>
@@ -196,47 +191,34 @@
    !   1.4 is no longer used the extclasspath type can be replaced by a simple
    !   path.
    !-->
-  <extclasspath id="installed.classpath">
-
-    <!-- Array -->
-    <pathelement location="${star.jar.dir}/array/array.jar"/>
- 
-    <!-- Table -->
-    <pathelement location="${star.jar.dir}/table/table.jar"/>
- 
-    <!-- TAMFITS -->
-    <pathelement location="${star.jar.dir}/tamfits/tamfits.jar"/>
-
-    <!-- FITS -->
-    <pathelement location="${star.jar.dir}/fits/fits.jar"/>
-
-    <!-- VOTable -->
-    <pathelement location="${star.jar.dir}/votable/votable.jar"/>
-
-    <!-- Util -->
-    <pathelement location="${star.jar.dir}/util/util.jar"/>
+  <path id="installed.classpath">
+    <pathelement location="${star.jar.dir}/starlink-array.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-connect.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-table.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-fits.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-votable.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-util.jar"/>
+    <pathelement location="${star.jar.dir}/fits.jar"/>
+    <pathelement location="${star.jar.dir}/commons-net.jar"/>
+  </path>
 
-  </extclasspath>
+  <path id="jar.classpath">
+    <pathelement location="${dist.lib.pkg}/starlink-array.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-connect.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-table.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-fits.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-votable.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-util.jar"/>
+    <pathelement location="${dist.lib.pkg}/fits.jar"/>
+    <pathelement location="${dist.lib.pkg}/commons-net.jar"/>
+  </path>
 
   <!-- Local build system jar files.
    !
    !   Name all the jar files of other packages that we depend on, which have
    !   not been installed (should be same packages as in installed.classpath).
    !-->
-  <extclasspath id="built.jarpath">
-
-    <pathelement
-       location="${star.build.dir}/array/lib/array/array.jar"/>
-    <pathelement
-       location="${star.build.dir}/table/lib/table/table.jar"/>
-    <pathelement
-       location="${star.build.dir}/tamfits/lib/tamfits/tamfits.jar"/>
-    <pathelement
-       location="${star.build.dir}/votable/lib/votable/votable.jar"/>
-    <pathelement
-       location="${star.build.dir}/util/lib/util/util.jar"/>
-
-  </extclasspath>
+  <path id="built.jarpath"/>
 
   <!-- Find all local third party jars files.
    !
@@ -453,7 +435,8 @@
            destdir="${build.classes}"
            debug="${debug}"
            deprecation="${deprecation}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            optimize="${optimize}">
 
       <classpath refid="classpath"/>
@@ -508,6 +491,10 @@
           description="-> creates the package jar file(s)">
 
     <mkdir dir="${dist.lib.pkg}"/>
+    <manifestclasspath property="jar.class.path"
+                       jarfile="${dist.lib.pkg}/${name}.jar">
+      <classpath refid="jar.classpath" />
+    </manifestclasspath>
     <jar destfile="${dist.lib.pkg}/${name}.jar"
          basedir="${build.classes}">
       <manifest>
@@ -516,17 +503,6 @@
       </manifest>
     </jar>
 
-    <!-- Sign all jar files -->
-    <antcall target="signjars"/>
-  </target>
-     
-  <target name="signjars" if="sign.jars">
-    <signjar alias="${webstart.alias}"
-             keystore="${webstart.keystore}"
-             keypass="${webstart.keypass}"
-             storepass="${webstart.storepass}">
-       <fileset dir="${dist.lib}" includes="**/*.jar **/*.zip"/>
-    </signjar>
   </target>
 
   <!--
@@ -968,7 +944,7 @@
              windowtitle="${Name} API"
              doctitle="${Name}"
              defaultexcludes="yes"
-             source="${source.version}"
+             encoding="cp1252"
              classpathref="classpath">
       <arg value="-Xdoclint:all,-missing"/>
       <arg value="-quiet"/>
@@ -1064,7 +1040,8 @@
     <javac srcdir="${tests.dir}"
            destdir="${build.tests}"
            debug="${debug}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            deprecation="${deprecation}" >
 
       <classpath refid="tests-classpath"/>
