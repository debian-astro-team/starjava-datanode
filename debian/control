Source: starjava-datanode
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               debhelper-compat (= 13),
               dh-exec,
               javahelper
Build-Depends-Indep: default-jdk,
                     default-jdk-doc,
                     libcommons-compress-java,
                     libcommons-net-java,
                     libfits-java,
                     starlink-array-java,
                     starlink-connect-java,
                     starlink-fits-java,
                     starlink-table-java (>= 4.1~),
                     starlink-util-java (>= 1.0+2022.04.04~),
                     starlink-votable-java
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/starjava-datanode
Vcs-Git: https://salsa.debian.org/debian-astro-team/starjava-datanode.git
Homepage: https://github.com/Starlink/starjava/tree/master/datanode

Package: starlink-datanode-java
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         starlink-table-java (>= 4.1~),
         starlink-util-java (>= 1.0+2022.04.04~)
Description: Classes for hierarchical browsing of data structures
 Using the classes available in this package, hierarchical data structures
 can be viewed interactively.
 .
 The GUI side is based on a Swing JTree component, and the underlying data
 model is supplied by classes conforming to the package's DataNode interface.

Package: starlink-datanode-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Recommends: ${java:Recommends}
Description: Classes for hierarchical browsing of data structures (documentation)
 Using the classes available in this package, hierarchical data structures
 can be viewed interactively.
 .
 The GUI side is based on a Swing JTree component, and the underlying data
 model is supplied by classes conforming to the package's DataNode interface.
 .
 This package contains the JavaDoc documentation of the package.
